from nodeflux.post_processing.multi_post_processing_nfinfer \
     import PostProcessing
from nodeflux.post_processing.multi_post_processing_nfinfer import FrameGrabber
from freezegun import freeze_time
import datetime
import binascii
import multiprocessing
import os


class SampleMultiNFInfer(PostProcessing):
    def __init__(
            self,
            param_input=[]):
        PostProcessing.__init__(self)

    def get_visualization(self):
        return 2


def test_multi_nfinfer_memory_none():
    fr_process = FrameGrabber()
    assert fr_process.mem_input.setup_mmap() is None, \
        "should return None in mem_input"


def test_multi_nfinfer_spawn_detector():
    fr_process = FrameGrabber()
    fr_process.spawn_predictor()
    assert fr_process.model_serializer is not None, \
        "should not return None in model_serializer"


def test_multi_nfinfer_preprocessing_none():
    fr_process = FrameGrabber()
    assert fr_process.pre_processing(None) is None, \
        "should return None in preprocessing"


def test_multi_nfinfer_preprocessing_with_raw():
    fr_process = FrameGrabber()
    data = binascii.b2a_hex(os.urandom(100*100))
    assert fr_process.pre_processing(data) is data, \
        "should return data with binary randoms value "


def test_multi_nfinfer_video_memory_trail():
    fr_process = FrameGrabber()
    fr_process.reinitialize_mmap_input()
    fr_process.mem_input.proc_buffer = bytes([7, 2, 136])
    assert fr_process.mem_input.check_trail() is True, \
        "should return check_trail with 7, 2, and 136 in bytes"


def test_multi_nfinfer_get_frame_from_grabber_frame_none():
    fr_process = FrameGrabber()
    frame, _, _, _ = fr_process.get_frame_from_grabber()
    assert frame is None, "should return None in frame"


def test_multi_nfinfer_get_frame_from_grabber_frame_timestamp():
    fr_process = FrameGrabber()
    _, frame_timestamp, _, _ = fr_process.get_frame_from_grabber()
    assert frame_timestamp is not None, "should not return time"


def test_multi_nfinfer_get_frame_from_grabber_width():
    fr_process = FrameGrabber()
    _, _, width, _ = fr_process.get_frame_from_grabber()
    assert width is not None, "should not return None"


def test_multi_nfinfer_get_frame_from_grabber_height():
    fr_process = FrameGrabber()
    _, _, _, height = fr_process.get_frame_from_grabber()
    assert height is not None, "should not return None"


def test_multi_nfinfer_start_detector_none():
    fr_process = FrameGrabber()
    p = multiprocessing.Process(target=fr_process.start_detector)
    p.start()
    p.join(3)
    if p.is_alive():
        p.terminate()
    assert fr_process.mem_input.setup_mmap() is None, \
        "should return None in mem_input"


def test_multi_nfinfer_memory_trail():
    fr_process = SampleMultiNFInfer()
    fr_process.initialize_mmap()
    fr_process.mem_out.proc_buffer = bytes([7, 2, 136])
    assert fr_process.mem_out.check_trail() is True, \
        "should return check_trail with 7, 2, and 136 in bytes"


def test_multi_nfinfer_callback_none():
    fr_process = SampleMultiNFInfer()
    fr_process.callback(None)
    assert fr_process.thread_flag is None, \
        "should return None in callback when input is None"


def test_multi_nfinfer_preprocessing_with_raw():
    fr_process = SampleMultiNFInfer()
    data = binascii.b2a_hex(os.urandom(100*100))
    assert fr_process.pre_processing(data) is data, \
        "should return data with binary randoms value "


def test_multi_nfinfer_preprocessing_none():
    fr_process = SampleMultiNFInfer()
    assert fr_process.pre_processing(None) is None, \
        "should return None in preprocessing"


def test_multi_nfinfer_callback_none():
    fr_process = SampleMultiNFInfer()
    assert fr_process.callback(None) is None, "should return None callback"


def test_multi_nfinfer_get_visualization():
    fr_process = SampleMultiNFInfer()
    assert fr_process.get_visualization() == 2, \
        "should return visualization values = 2 "


def test_multi_nfinfer_run_preprocessing_none():
    fr_process = SampleMultiNFInfer()
    assert fr_process.run_processing(None, None, None) is None, \
        "should return None in processing"


def test_multi_nfinfer_write_restream():
    fr_process = SampleMultiNFInfer()
    p = multiprocessing.Process(target=fr_process.write_restream)
    p.start()
    p.join(3)
    if p.is_alive():
        p.terminate()
        pass
    else:
        assert None is False, \
            "should infinite loop when capture is activate"


def test_multi_nfinfer_run():
    fr_process = SampleMultiNFInfer()
    p = multiprocessing.Process(target=fr_process.run)
    p.start()
    p.join(3)
    if p.is_alive():
        p.terminate()
        pass
    else:
        assert None is False, \
            "should infinite loop when capture is activate"

