from nodeflux.memutils import *
from nodeflux.post_processing.post_processing_video import PostProcessing
from freezegun import freeze_time
from pytest_cov.embed import cleanup_on_sigterm
import datetime
import multiprocessing


class SampleAnalytic(PostProcessing):
    def __init__(
            self,
            param_input=[]):
        PostProcessing.__init__(
            self,
            end_point='rtsp://admin:nodeflux123@10.7.1.51:554',
            mmap_size=(720 * 1280 * 3) + 8000,
            width=720,
            height=1280,
        )

    def get_visualization(self):
        PostProcessing.get_visualization(self)
        return None

    def run_processing(self, img, callback=None):
        PostProcessing.run_processing(None, None, None)
        pass


@freeze_time("2019-04-8")
def test_video_timestamp():
    fr_process = SampleAnalytic()
    assert fr_process.get_frame_timestamp() == datetime.datetime(2019, 4, 8)


def test_video_memory_none():
    fr_process = SampleAnalytic()
    fr_process.mem_in.flush_mmap()
    fr_process.mem_out.flush_mmap()
    fr_process.mem_raw.flush_mmap()
    fr_process.mem_raw_raw.flush_mmap()

    cols = (
        fr_process.mem_in,
        fr_process.mem_out,
        fr_process.mem_raw,
        fr_process.mem_raw_raw
    )

    result = bytes([0])
    for col in cols:
        col.proc_buffer = bytes([12])
        print(col.proc_buffer[:3])
        result = col.check_trail()

    assert result is None


def test_video_preprocessing_none():
    fr_process = SampleAnalytic()
    assert fr_process.pre_processing(None) is None


def test_video_get_visualization_none():
    fr_process = SampleAnalytic()
    assert fr_process.get_visualization() is None


def test_video_run_preprocessing_none():
    fr_process = SampleAnalytic()
    assert fr_process.run_processing(None, None) is None


def test_video_callback_none():
    fr_process = SampleAnalytic()
    assert fr_process.callback(None) is None


def test_video_calculate_fps():
    fr_process = SampleAnalytic()
    fr_process.callback(None)
    assert fr_process.run_processing(None, None) is None


def test_video_run():
    fr_process = SampleAnalytic()
    p = multiprocessing.Process(target=fr_process.run)
    p.start()
    p.join(1)
    if p.is_alive():
        cleanup_on_sigterm()
        p.terminate()
        pass
    else:
        assert None is False, \
            "should infinite loop when capture is activate"

